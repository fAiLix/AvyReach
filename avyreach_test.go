package main

import (
	"path/filepath"
	"reflect"
	"runtime"
	"testing"
)

/******************************************
* Tests
 ******************************************/

/******************************************
* Test Helpers
 ******************************************/
func ok(tb testing.TB, err error) {
	tb.Helper()
	if err != nil {
		_, file, line, _ := runtime.Caller(1)
		tb.Errorf("%s:%d: unexpected error: %s\n\n", filepath.Base(file), line, err.Error())
	}
}

// equals fails the test if exp is not equal to act.
func equals(tb testing.TB, exp, act interface{}) {
	tb.Helper()
	if !reflect.DeepEqual(exp, act) {
		_, file, line, _ := runtime.Caller(1)
		tb.Errorf("%s:%d:\n\n\texp: %#v\n\n\tgot: %#v\n\n", filepath.Base(file), line, exp, act)
	}
}
