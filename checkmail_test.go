package main

import (
	"testing"
)

/******************************************
* Tests
 ******************************************/
func TestExtractURL(t *testing.T) {
	myString := "Please send the avalanche bulletin\nView the location or send a reply:\nhttps://eur.explore.garmin.com/textmessage/txtmsg?extId=f9a31e20-f770-4ed1-81ec-3768127029e7&adr=test%40example.com"

	got := extractURL(myString)
	want := "https://eur.explore.garmin.com/textmessage/txtmsg?extId=f9a31e20-f770-4ed1-81ec-3768127029e7&adr=test%40example.com"

	if got != want {
		t.Errorf("got %q want %q", got, want)
	}
}

func TestExtractRegionID(t *testing.T) {
	myString := "Jotunheimen 3028\nView the location or send a reply:\nhttps://eur.explore.garmin.com/textmessage/txtmsg?extId=f9a31e20-f770-4ed1-81ec-3768127029e7&adr=test%40example.com"

	got := extractRegionID(myString)
	want := "3028"

	if got != want {
		t.Errorf("got %q want %q", got, want)
	}
}
