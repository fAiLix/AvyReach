# AvyReach
AvyReach is a small go programm which listens on an email address for messages from an InReach device and will answer with the current avalanche-bulletin for the specified region in norway.

available regions and their associated ids can be found on the [documentation page of the norwegian avalanche bulletin api](http://api.nve.no/doc/snoeskredvarsel/#avalanchewarningbyregionsimple_regionid)

To decode the shortened bulletin messages, see [this repository](https://codeberg.org/fAiLix/AvyReach-Report) for more information.

## Building

To build the app, run `go build avyreach.go checkmail.go`

## Starting the app
To start the app, set the required environment variables and launch the app
If you have your env vars in a .env file, you can use the following command
`env $(cat settings.env | xargs) ./avyreach`

### Environment variables
Environment variables which are needed for the app to work:
- MAIL_HOST
- MAIL_USER
- MAIL_PWD
- MAIL_FOLDER

## Docker

To build the image:
`docker build -t avyreach .`

To run the container:
`docker run --rm --env-file settings.env avyreach`

# ToDo
- [ ] Package for email operations
    - [x] extract URL from the Mail
    - [x] extract region from the Mail
    - [x] Imap Idle waiting for messages
    - [x] Fetch new messages from server
    - [ ] Implement tests for checkmail
- [x] Package for the avalanche bulletin
    - [x] Get the Norwegian avalanche bulletin by Reqion
    - [x] Shorten the bulletin
- [x] Package for InReach Stuff
    - [x] Generate the API-URL
    - [x] Create the API-Call
    - [x] Split long messages in junks of 160 chars
    - [x] Check for the max message size
    - [x] Send message to the InReach
- [ ] Proper error handling