FROM golang:latest as builder
WORKDIR /app
COPY . .
RUN CGO_ENABLED=0 go build -tags timetzdata -o avyreach .
# Create an unprivileged user
RUN adduser \
    --disabled-password \
    --gecos "avyreach" \
    --shell "/sbin/nologin" \
    --no-create-home \
    --uid "901" \
    "avyreach"

FROM scratch
# Copy the tls certificates:
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
# Install the unprivileged user in the container
COPY --from=builder /etc/passwd /etc/passwd
COPY --from=builder /etc/group /etc/group
USER avyreach:avyreach

# Copy the application:
COPY --from=builder /app/avyreach .
CMD ["/avyreach"]