package bulletin

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"os"
	"path/filepath"
	"reflect"
	"runtime"
	"testing"
)

func TestGetBulletin(t *testing.T) {
	testExecuted := false

	bulletinFile := bulletinFromFile(t, "test_bulletin.json")
	bulletinReader, err := json.Marshal(bulletinFile)
	if err != nil {
		t.Errorf("Unable to create reader from JSON")
	}

	client := NewTestClient(func(req *http.Request) *http.Response {
		// Test request parameters
		equals(t, "https://api01.nve.no/hydrology/forecast/avalanche/v5.0.1/api/AvalancheWarningByRegion/Detail/3028/2/", req.URL.String())
		testExecuted = true

		return &http.Response{
			StatusCode: 200,
			// Send response to be tested
			Body: ioutil.NopCloser(bytes.NewReader(bulletinReader)),
			// Must be set to non-nil value or it panics
			Header: make(http.Header),
		}
	})

	bulletin, err := Get(client, "3028")
	ok(t, err)
	equals(t, bulletinFile, bulletin)
	if !testExecuted {
		t.Error("HTTP Client never called")
	}
}

func TestShortenBulletin(t *testing.T) {
	// Read JSON bulletin from file
	bulletin := bulletinFromFile(t, "test_bulletin.json")

	shortBulletin := Shorten(bulletin)
	exptected := "{2[13,21,30,11100011,1400,1400][24,22,5,00111110,1500,1500]}{2[13,21,30,11100011,1400,1400][24,22,5,00111110,1500,1500]}"
	equals(t, exptected, shortBulletin)
}

/******************************************
* Test Helpers
 ******************************************/
func ok(tb testing.TB, err error) {
	tb.Helper()
	if err != nil {
		_, file, line, _ := runtime.Caller(1)
		tb.Errorf("%s:%d: unexpected error: %s\n\n", filepath.Base(file), line, err.Error())
	}
}

// equals fails the test if exp is not equal to act.
func equals(tb testing.TB, exp, act interface{}) {
	tb.Helper()
	if !reflect.DeepEqual(exp, act) {
		_, file, line, _ := runtime.Caller(1)
		tb.Errorf("%s:%d:\n\n\texp: %#v\n\n\tgot: %#v\n\n", filepath.Base(file), line, exp, act)
	}
}

func bulletinFromFile(tb testing.TB, file string) Bulletin {
	tb.Helper()
	jsonFile, err := os.Open(file)
	if err != nil {
		tb.Errorf("Unable to open required file %s", file)
	}
	defer jsonFile.Close()
	data, err := ioutil.ReadAll(jsonFile)
	var bulletin Bulletin
	json.Unmarshal(data, &bulletin)

	return bulletin
}

// RoundTripFunc .
type RoundTripFunc func(req *http.Request) *http.Response

// RoundTrip .
func (f RoundTripFunc) RoundTrip(req *http.Request) (*http.Response, error) {
	return f(req), nil
}

//NewTestClient returns *http.Client with Transport replaced to avoid making real calls
func NewTestClient(fn RoundTripFunc) *http.Client {
	return &http.Client{
		Transport: RoundTripFunc(fn),
	}
}
