package bulletin

// Bulletin is the container for the norwegian avalanche bulletin
type Bulletin []struct {
	Author           string `json:"Author"`
	AvalancheAdvices []struct {
		AdviceID int64  `json:"AdviceID"`
		ImageURL string `json:"ImageUrl"`
		Text     string `json:"Text"`
	} `json:"AvalancheAdvices"`
	AvalancheDanger   string `json:"AvalancheDanger"`
	AvalancheProblems []struct {
		AvalCauseID              int64  `json:"AvalCauseId"`
		AvalCauseName            string `json:"AvalCauseName"`
		AvalProbabilityID        int64  `json:"AvalProbabilityId"`
		AvalProbabilityName      string `json:"AvalProbabilityName"`
		AvalPropagationID        int64  `json:"AvalPropagationId"`
		AvalPropagationName      string `json:"AvalPropagationName"`
		AvalTriggerSimpleID      int64  `json:"AvalTriggerSimpleId"`
		AvalTriggerSimpleName    string `json:"AvalTriggerSimpleName"`
		AvalancheExtID           int64  `json:"AvalancheExtId"`
		AvalancheExtName         string `json:"AvalancheExtName"`
		AvalancheProblemID       int64  `json:"AvalancheProblemId"`
		AvalancheProblemTypeID   int64  `json:"AvalancheProblemTypeId"`
		AvalancheProblemTypeName string `json:"AvalancheProblemTypeName"`
		AvalancheTypeID          int64  `json:"AvalancheTypeId"`
		AvalancheTypeName        string `json:"AvalancheTypeName"`
		DestructiveSizeExtID     int64  `json:"DestructiveSizeExtId"`
		DestructiveSizeExtName   string `json:"DestructiveSizeExtName"`
		ExposedHeight1           int64  `json:"ExposedHeight1"`
		ExposedHeight2           int64  `json:"ExposedHeight2"`
		ExposedHeightFill        int64  `json:"ExposedHeightFill"`
		ValidExpositions         string `json:"ValidExpositions"`
	} `json:"AvalancheProblems"`
	CountyList []struct {
		ID   string `json:"Id"`
		Name string `json:"Name"`
	} `json:"CountyList"`
	CurrentWeaklayers       string `json:"CurrentWeaklayers"`
	DangerLevel             string `json:"DangerLevel"`
	DangerLevelName         string `json:"DangerLevelName"`
	EmergencyWarning        string `json:"EmergencyWarning"`
	LangKey                 int64  `json:"LangKey"`
	LatestAvalancheActivity string `json:"LatestAvalancheActivity"`
	LatestObservations      string `json:"LatestObservations"`
	MainText                string `json:"MainText"`
	MountainWeather         struct {
		CloudCoverID     int64  `json:"CloudCoverId"`
		CloudCoverName   string `json:"CloudCoverName"`
		Comment          string `json:"Comment"`
		LastSavedTime    string `json:"LastSavedTime"`
		MeasurementTypes []struct {
			ID                  int64 `json:"Id"`
			MeasurementSubTypes []struct {
				ID        int64  `json:"Id"`
				Name      string `json:"Name"`
				SortOrder int64  `json:"SortOrder"`
				Value     string `json:"Value"`
			} `json:"MeasurementSubTypes"`
			Name      string `json:"Name"`
			SortOrder int64  `json:"SortOrder"`
		} `json:"MeasurementTypes"`
	} `json:"MountainWeather"`
	MunicipalityList []struct {
		ID   string `json:"Id"`
		Name string `json:"Name"`
	} `json:"MunicipalityList"`
	NextWarningTime      string `json:"NextWarningTime"`
	PreviousWarningRegID int64  `json:"PreviousWarningRegId"`
	PublishTime          string `json:"PublishTime"`
	RegID                int64  `json:"RegId"`
	RegionID             int64  `json:"RegionId"`
	RegionName           string `json:"RegionName"`
	RegionTypeID         int64  `json:"RegionTypeId"`
	RegionTypeName       string `json:"RegionTypeName"`
	SnowSurface          string `json:"SnowSurface"`
	UtmEast              int64  `json:"UtmEast"`
	UtmNorth             int64  `json:"UtmNorth"`
	UtmZone              int64  `json:"UtmZone"`
	ValidFrom            string `json:"ValidFrom"`
	ValidTo              string `json:"ValidTo"`
}
