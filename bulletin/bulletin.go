package bulletin

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"strconv"
)

// Get takes a http.Client reference and a region string according to http://api.nve.no/doc/snoeskredvarsel/#avalanchewarningbyregiondetail_regionid
// returns a Bulletin object
func Get(client *http.Client, region string) (Bulletin, error) {
	apiURL := "https://api01.nve.no/hydrology/forecast/avalanche/v5.0.1/api/AvalancheWarningByRegion/Detail/" + region + "/2/"
	resp, err := client.Get(apiURL)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	// var result []map[string]interface{}
	var bulletin Bulletin
	json.Unmarshal(data, &bulletin)

	return bulletin, nil
}

// Shorten takes a Bulletin object and returns a short version of the bulletin as string
func Shorten(bulletin Bulletin) string {
	var out string

	for _, bullet := range bulletin {
		dangerLvl, _ := strconv.Atoi(bullet.DangerLevel)
		if dangerLvl != 0 {
			out += "{"
			out += bullet.DangerLevel

			if bullet.AvalancheProblems != nil { // check if there are avalance Problems
				for _, problem := range bullet.AvalancheProblems {

					out += "[" +
						strconv.Itoa(int(problem.AvalCauseID)) + "," +
						strconv.Itoa(int(problem.AvalTriggerSimpleID)) + "," +
						strconv.Itoa(int(problem.AvalancheProblemTypeID)) + "," +
						problem.ValidExpositions + "," +
						strconv.Itoa(int(problem.ExposedHeight1)) + "," +
						strconv.Itoa(int(problem.ExposedHeight2)) +
						"]"
				}
			}
			out += "}"
		}
	}

	return out
}
