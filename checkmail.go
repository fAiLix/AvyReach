package main

import (
	"log"
	"net/http"
	"regexp"

	idle "github.com/emersion/go-imap-idle"
	"github.com/emersion/go-imap/client"
	"github.com/fail-ix/avyreach/bulletin"
	"github.com/fail-ix/avyreach/inreach"
	"github.com/jprobinson/eazye"
	"mvdan.cc/xurls/v2"
)

type pair struct {
	IR       inreach.InReach
	RegionID string
}

// Todo: make the error channel external
func idleClient(c *client.Client, updates chan client.Update, done chan error) {
	idleClient := idle.NewClient(c)

	c.Updates = updates

	// Start idling
	go func() {
		done <- idleClient.IdleWithFallback(nil, 0)
	}()
	log.Println("Idling now...")
}

func searchFROM(cfg eazye.MailboxInfo, mailAddress string) ([]eazye.Email, error) {
	emails, err := eazye.GetCommand(cfg, "UNSEEN FROM "+mailAddress, true, true)
	if err != nil {
		return nil, err
	}

	return emails, nil
}

func extractMSG(mails []eazye.Email) map[string]string {
	// Create the map for the URLs we want to return
	URLsToSend := make(map[string]string, 1)

	for _, mail := range mails {
		message := string(mail.Text)
		url := extractURL(message)
		regionID := extractRegionID(message)
		URLsToSend[url] = regionID
	}
	return URLsToSend
}

// extractURL takes a string and returns the first URL in the String
func extractURL(msg string) string {
	rxStrict := xurls.Strict()
	url := rxStrict.FindString(msg)
	return url
}

// extractURL takes a string and returns the first URL in the String
func extractRegionID(msg string) string {
	re := regexp.MustCompile(`\b\d{4}\b`)
	regionID := re.FindString(msg)
	return regionID
}

func mailWorker(cfg eazye.MailboxInfo, updates chan client.Update, mailAddress string) {
	for update := range updates {
		switch update.(type) {
		case *client.MailboxUpdate:
			ids, err := searchFROM(cfg, mailAddress)
			if err != nil {
				log.Fatal(err)
			}
			URLsToSend := extractMSG(ids)
			devices := createDevices(URLsToSend)
			sendBulletin(devices)
		}
	}
}

func createDevices(URLsToSend map[string]string) []pair {
	var devicesToNotify []pair
	for key, val := range URLsToSend {
		var ir inreach.InReach
		ir.SetURL(key)
		devicesToNotify = append(devicesToNotify, pair{ir, val})
	}
	return devicesToNotify
}

func fetchBulletin(regionID string) string {
	c := &http.Client{}
	b, err := bulletin.Get(c, regionID)
	if err != nil {
		log.Printf("Error getting bulletin: %v", b)
	}
	return bulletin.Shorten(b)
}

func sendBulletin(devices []pair) {
	c := &http.Client{}
	for _, inReach := range devices {
		b := fetchBulletin(inReach.RegionID)
		err := inReach.IR.Send(c, b)
		if err != nil {
			log.Println("Unable to send bulletin:")
			log.Println(err.Error())
		} else {
			log.Println("Bulletin sent!")
		}
	}
}
