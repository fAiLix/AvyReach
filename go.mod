module github.com/fail-ix/avyreach

go 1.14

require (
	github.com/emersion/go-imap v1.2.0
	github.com/emersion/go-imap-idle v0.0.0-20210907174914-db2568431445
	github.com/emersion/go-sasl v0.0.0-20211008083017-0b9dcfb154ac // indirect
	github.com/jprobinson/eazye v0.0.0-20200316195029-00167c745a93
	github.com/mxk/go-imap v0.0.0-20150429134902-531c36c3f12d // indirect
	github.com/paulrosania/go-charset v0.0.0-20190326053356-55c9d7a5834c // indirect
	github.com/sloonz/go-qprintable v0.0.0-20210417175225-715103f9e6eb // indirect
	golang.org/x/net v0.0.0-20211216030914-fe4d6282115f // indirect
	mvdan.cc/xurls/v2 v2.3.0
)
