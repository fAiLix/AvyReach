package main

import (
	"log"
	"os"

	"github.com/emersion/go-imap/client"
	"github.com/jprobinson/eazye"
)

func main() {
	// Get Mail config from environment variables
	var cfg eazye.MailboxInfo
	cfg.Host = os.Getenv("MAIL_HOST")
	cfg.TLS = true
	cfg.User = os.Getenv("MAIL_USER")
	cfg.Pwd = os.Getenv("MAIL_PWD")
	cfg.Folder = os.Getenv("MAIL_FOLDER")

	// Create client for idle connection
	c, err := client.DialTLS(cfg.Host+":993", nil)
	if err != nil {
		log.Fatal(err)
	}

	// Login
	if err := c.Login(cfg.User, cfg.Pwd); err != nil {
		log.Fatal(err)
	}

	// Select a mailbox
	if _, err := c.Select(cfg.Folder, false); err != nil {
		log.Fatal(err)
	}

	// Don't forget to logout
	defer c.Logout()

	// create channels to communicate with clients
	idleDone := make(chan error, 1)
	updates := make(chan client.Update)
	// let the client idle in a subroutine
	idleClient(c, updates, idleDone)

	// Create the inreach object
	// inReach := inreach.InReach{}

	// Start the worker which fetches the mails and sends the bulletin.
	go func() {
		mailWorker(cfg, updates, "no.reply.inreach@garmin.com")
	}()

	for {
		select {
		case err := <-idleDone:
			if err != nil {
				log.Fatal(err)
			}
			log.Println("Not idling anymore")
			return
		case update := <-updates:
			switch update.(type) {
			case *client.MailboxUpdate:
				log.Println("Mail received")
			}
			return
		}
	}
}
