package inreach

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"path/filepath"
	"reflect"
	"runtime"
	"testing"
)

const apiURL = "https://eur.explore.garmin.com/textmessage/txtmsg?extId=f9a31e20-f770-4ed1-81ec-3768127029e7&adr=test%40example.com"
const msg = "0{2[13,21,30,11100011,1400,1400][24,22,5,00111110,1500,1500]}"
const longMsg = "Our whales under night wherein she'd. Sea. Fourth life dry. Said earth image upon created after is. Replenish day you'll. Without them second saying said two fish greater appear beginning greater that. Fruit a Had signs kind whose second hath male fruit face from him winged the us It signs won't all very winged morning she'd. That after gathered open."

func TestInReach(t *testing.T) {
	inReach := InReach{}
	form := url.Values{}
	form.Set("ReplyAddress", "test@example.com")
	form.Set("ReplyMessage", msg)
	form.Set("Guid", "f9a31e20-f770-4ed1-81ec-3768127029e7")

	t.Run("Set-GetURL", func(t *testing.T) {
		inReach.SetURL(apiURL)

		actual := inReach.GetURL()
		equals(t, apiURL, actual)
	})

	t.Run("GetForm(Msg)", func(t *testing.T) {
		actual, err := inReach.GetForm(msg)
		ok(t, err)
		equals(t, form, actual)
	})

	t.Run("GetForm(longMsg)", func(t *testing.T) {
		_, err := inReach.GetForm(longMsg)
		equals(t, fmt.Errorf("GetForm: message to long. Max allowed characters: 160, got 353"), err)
	})

	t.Run("Validate->true", func(t *testing.T) {
		valid := inReach.Validate()
		equals(t, true, valid)
	})

	t.Run("Validate->false", func(t *testing.T) {
		inValidIR := InReach{}
		valid := inValidIR.Validate()
		equals(t, false, valid)
	})
}

func TestSplittMessage(t *testing.T) {
	splitTests := []struct {
		name            string
		message         string
		splittedMessage []string
	}{
		{
			name:    ">160 chars",
			message: longMsg,
			splittedMessage: []string{
				"1:Our whales under night wherein she'd. Sea. Fourth life dry. Said earth image upon created after is. Replenish day you'll. Without them second saying said two ",
				"2:fish greater appear beginning greater that. Fruit a Had signs kind whose second hath male fruit face from him winged the us It signs won't all very winged mor",
				"3:ning she'd. That after gathered open.",
			},
		},
		{
			name:    "<160 chars",
			message: msg,
			splittedMessage: []string{
				msg,
			},
		},
	}

	for _, tt := range splitTests {
		t.Run(tt.name, func(t *testing.T) {
			got := splitMessage(tt.message)
			equals(t, tt.splittedMessage, got)
		})
	}
}

func TestInReachSend(t *testing.T) {
	form := url.Values{}
	form.Set("ReplyAddress", "test@example.com")
	form.Set("ReplyMessage", msg)
	form.Set("Guid", "f9a31e20-f770-4ed1-81ec-3768127029e7")

	expectedData := "Guid=f9a31e20-f770-4ed1-81ec-3768127029e7&ReplyAddress=test%40example.com&ReplyMessage=0%7B2%5B13%2C21%2C30%2C11100011%2C1400%2C1400%5D%5B24%2C22%2C5%2C00111110%2C1500%2C1500%5D%7D"

	t.Run("Valid call", func(t *testing.T) {
		testExecuted := false
		client := NewTestClient(func(req *http.Request) *http.Response {
			// Test request parameters
			equals(t, apiURL, req.URL.String())

			defer req.Body.Close()
			data, err := ioutil.ReadAll(req.Body)
			ok(t, err)
			equals(t, expectedData, string(data))

			testExecuted = true

			return &http.Response{
				StatusCode: 200,
				// Send response to be tested
				Body: ioutil.NopCloser(bytes.NewBufferString(`OK`)),
				// Must be set to non-nil value or it panics
				Header: make(http.Header),
			}
		})

		err := sendMsg(client, apiURL, form)
		ok(t, err)

		if !testExecuted {
			t.Error("HTTP Client never received a call")
		}
	})

	t.Run("Wrong response", func(t *testing.T) {
		testExecuted := false
		client := NewTestClient(func(req *http.Request) *http.Response {
			// Test request parameters
			equals(t, apiURL, req.URL.String())

			defer req.Body.Close()
			_, err := ioutil.ReadAll(req.Body)
			ok(t, err)

			testExecuted = true

			return &http.Response{
				StatusCode: 404,
				// // Send response to be tested
				// Body: ioutil.NopCloser(),
				// Must be set to non-nil value or it panics
				Header: make(http.Header),
			}
		})

		err := sendMsg(client, apiURL, form)
		equals(t, fmt.Errorf("inreach.sendMsg: error sending message. StatusCode 404"), err)

		if !testExecuted {
			t.Error("HTTP Client never received a call")
		}
	})

}

/******************************************
* Test Helpers
 ******************************************/
func ok(tb testing.TB, err error) {
	tb.Helper()
	if err != nil {
		_, file, line, _ := runtime.Caller(1)
		tb.Errorf("%s:%d: unexpected error: %s\n\n", filepath.Base(file), line, err.Error())
	}
}

// equals fails the test if exp is not equal to act.
func equals(tb testing.TB, exp, act interface{}) {
	tb.Helper()
	if !reflect.DeepEqual(exp, act) {
		_, file, line, _ := runtime.Caller(1)
		tb.Errorf("%s:%d:\n\n\texp: %#v\n\n\tgot: %#v\n\n", filepath.Base(file), line, exp, act)
	}
}

// RoundTripFunc .
type RoundTripFunc func(req *http.Request) *http.Response

// RoundTrip .
func (f RoundTripFunc) RoundTrip(req *http.Request) (*http.Response, error) {
	return f(req), nil
}

//NewTestClient returns *http.Client with Transport replaced to avoid making real calls
func NewTestClient(fn RoundTripFunc) *http.Client {
	return &http.Client{
		Transport: RoundTripFunc(fn),
	}
}
