// Package inreach provides functions to send a message to an InReach device
package inreach

import (
	"fmt"
	"log"
	"net/http"
	"net/url"
	"strconv"
)

// InReach holds the Data for the API call
type InReach struct {
	host  string
	extID string
	mail  string
}

// SetURL takes the the link for replying to messages
// and populates all needed values of the object.
func (i *InReach) SetURL(link string) {
	urlArg, err := url.Parse(link) // read the url argument and parse it
	if err != nil {
		log.Fatal(err)
	}

	i.host = urlArg.Host
	query, err := url.ParseQuery(urlArg.RawQuery)
	if err != nil {
		log.Fatal(err)
	}
	i.extID = query["extId"][0]
	i.mail = query["adr"][0]
}

// GetURL returns the URL to which the message request needs to be send.
func (i *InReach) GetURL() string {
	return "https://" + url.QueryEscape(i.host) + "/textmessage/txtmsg?extId=" + url.QueryEscape(i.extID) + "&adr=" + url.QueryEscape(i.mail)
}

// GetForm takes a message String and returns a url.Values object filled with all necessary values.
// If the supplied message is longer than 160 charactes (maximum allowed message for the InReach) it returns an error.
func (i *InReach) GetForm(msg string) (url.Values, error) {
	if len(msg) > 160 {
		return nil, fmt.Errorf("GetForm: message to long. Max allowed characters: 160, got %d", len(msg))
	}
	return url.Values{
		"Guid":         []string{i.extID},
		"ReplyAddress": []string{i.mail},
		"ReplyMessage": []string{msg},
	}, nil
}

// Validate checks if all fields of the object are populated and returns a bool.
func (i *InReach) Validate() bool {
	return i.extID != "" && i.host != "" && i.mail != ""
}

// splittMessage takes a string, splits it at length 158 and numbers it.
// Returns a slice with the numbered and splited messages
func splitMessage(msg string) []string {
	var out []string
	if len(msg) > 160 {
		iterations := len(msg) / 158

		for i := 0; i <= iterations; i++ {
			beginn := i * 158
			end := (i + 1) * 158
			var shortMsg string
			if len(msg) > end {
				shortMsg = strconv.Itoa(i+1) + ":" + msg[beginn:end]
			} else {
				shortMsg = strconv.Itoa(i+1) + ":" + msg[beginn:]
			}
			out = append(out, shortMsg)
		}
	} else {
		out = append(out, msg)
	}
	return out
}

func sendMsg(client *http.Client, apiURL string, form url.Values) error {
	resp, err := client.PostForm(apiURL, form)
	if err != nil {
		return err
	}
	if resp.StatusCode != 200 {
		return fmt.Errorf("inreach.sendMsg: error sending message. StatusCode %d", resp.StatusCode)
	}

	return nil
}

// Send takes a message and sends it to an InReach device
// returns an error on failure
func (i *InReach) Send(client *http.Client, msg string) error {
	if i.Validate() {
		splitedMsg := splitMessage(msg)
		apiURL := i.GetURL()

		for _, message := range splitedMsg {
			form, err := i.GetForm(message)
			if err != nil {
				return err
			}
			err = sendMsg(client, apiURL, form)
			if err != nil {
				return err
			}
		}
		return nil
	}
	return fmt.Errorf("Send: error not all fields specified")
}
